package promorepo

import (
	"cart-system/entity"
	"context"
)

func (s *samplePromoRepo) item_bought_by_x_eligible(ctx context.Context, pr entity.PromoRule, cart entity.Cart) (bool, error) {
	eligible := false

	for _, cartLine := range cart.CartLines {
		if cartLine.ProductID == *pr.ProductID && cartLine.Quantity >= *pr.XCount {
			eligible = true
			break
		}
	}

	return eligible, nil
}
