package promorepo

import (
	"cart-system/entity"
	"context"
)

type (
	PromoRepo interface {
		GetPromoByCode(ctx context.Context, code string) (entity.Promo, error)
		IsEligible(ctx context.Context, p entity.Promo, cart entity.Cart) (bool, error)
		PromoActivate(ctx context.Context, p entity.Promo, cart *entity.Cart) error
	}
)
