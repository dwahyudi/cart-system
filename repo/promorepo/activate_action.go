package promorepo

import (
	"cart-system/entity"
	"cart-system/service/calc"
	"context"
	"fmt"

	"github.com/oklog/ulid/v2"
)

func (s *samplePromoRepo) free_item_activate(ctx context.Context, promo entity.Promo, pa entity.PromoAction, cart *entity.Cart) error {
	var refCartLine entity.CartLine
	for _, cartLine := range cart.CartLines {
		if cartLine.ProductID == *pa.ProductRefID {
			refCartLine = *cartLine
		}
	}

	eachFreeQuantity := pa.XCount
	totalFreeQuantity := *eachFreeQuantity * refCartLine.Quantity
	newCartLine := entity.CartLine{
		ID:        ulid.Make().String(),
		ProductID: *pa.ProductID,
		Quantity:  totalFreeQuantity,
		Subtotal:  0.0,
		FreeItem:  true,
	}

	cart.CartLines = append(cart.CartLines, &newCartLine)

	return nil
}

func (s *samplePromoRepo) item_discount_for_x(ctx context.Context, promo entity.Promo, pa entity.PromoAction, cart *entity.Cart) error {
	var activableCartLine entity.CartLine
	quantityOfDiscount := 0.0
	for _, cartLine := range cart.CartLines {
		if cartLine.ProductID == *pa.ProductID {
			activableCartLine = *cartLine
		}
	}

	eachItemDiscount := activableCartLine.ProductSnapshot.Price * (*pa.DiscountPercentage / 100)

	if activableCartLine.Quantity > *pa.XCount {
		quantityOfDiscount = *pa.XCount
	} else {
		quantityOfDiscount = activableCartLine.Quantity
	}

	totalDiscount := calc.RoundFloat(eachItemDiscount*quantityOfDiscount, 2)

	cart.Discounts = append(cart.Discounts, entity.Discount{
		ID:          ulid.Make().String(),
		Description: fmt.Sprintf("Promo %s", promo.Name),
		Amount:      totalDiscount,
	})

	return nil
}
