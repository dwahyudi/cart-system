package promorepo

import (
	"cart-system/entity"
	"context"
)

type (
	samplePromoRepo struct {
	}
)

func (s *samplePromoRepo) GetPromoByCode(ctx context.Context, code string) (entity.Promo, error) {
	var promo entity.Promo

	for _, samplePromo := range s.samplePromos(ctx) {
		if samplePromo.Code == code {
			promo = samplePromo
		}
	}

	return promo, nil
}

func (s *samplePromoRepo) IsEligible(ctx context.Context, p entity.Promo, cart entity.Cart) (bool, error) {
	eligible := false

	for _, promoRule := range p.PromoRules {
		ruleEligible, err := s.RuleIsEligible(ctx, promoRule, cart)
		if err != nil {
			return false, err
		}

		if p.MustSatisfyAllRules {
			if !ruleEligible {
				eligible = false
				break
			}
		} else {
			if ruleEligible {
				eligible = true
				break
			}
		}
	}

	return eligible, nil
}

func (s *samplePromoRepo) RuleIsEligible(ctx context.Context, pr entity.PromoRule, cart entity.Cart) (bool, error) {
	eligible := false

	switch pr.Type {
	case entity.ITEM_BOUGHT_BY_X:
		return s.item_bought_by_x_eligible(ctx, pr, cart)
	}

	return eligible, nil
}

func (s *samplePromoRepo) PromoActivate(ctx context.Context, p entity.Promo, cart *entity.Cart) error {
	for _, promoAction := range p.PromoActions {
		s.ActionActivate(ctx, p, promoAction, cart)
	}

	cart.PromoID = p.ID

	return nil
}

func (s *samplePromoRepo) ActionActivate(ctx context.Context, promo entity.Promo, pa entity.PromoAction, cart *entity.Cart) error {
	switch pa.Type {
	case entity.FREE_ITEM:
		s.free_item_activate(ctx, promo, pa, cart)
	case entity.ITEM_DISCOUNT_FOR_X:
		s.item_discount_for_x(ctx, promo, pa, cart)
	}

	return nil
}

func (s *samplePromoRepo) samplePromos(ctx context.Context) []entity.Promo {
	var (
		macbookID      = "01GCKY3CQC6J71F3WF99YS52EP"
		raspberryID    = "01GCKY4FPQX4HGM9P5A3MZZE97"
		googleHomeID   = "01GCKY3SEJWJRN4YM5ANS599QP"
		alexaSpeakerID = "01GCKY44256QSMAJDS73V2YAV2"

		value_1    = 1.0
		value_3    = 3.0
		value_10   = 10.0
		value_100  = 100.0
		value_9999 = 9999.0
	)

	return []entity.Promo{
		{
			ID:   "01GCKRESKRP8TS2RFH7YP12GYD",
			Name: "Each Macbook Get Raspberry Pi for Free",
			Code: "MACBOOK-01",
			PromoRules: []entity.PromoRule{
				{
					ID:        "01GCKY3CQC6J71F3WF99YS52EP",
					Type:      entity.ITEM_BOUGHT_BY_X,
					ProductID: &macbookID,
					XCount:    &value_1,
				},
			},
			PromoActions: []entity.PromoAction{
				{
					ID:           "01GCKY4FPQX4HGM9P5A3MZZE97",
					Type:         entity.FREE_ITEM,
					ProductID:    &raspberryID,
					ProductRefID: &macbookID,
					XCount:       &value_1,
				},
			},
		},
		{
			ID:   "01GCKRQ1QFRKXRPJYPK34PKVSF",
			Name: "Google Home Promo",
			Code: "GOOGLEHOME-01",
			PromoRules: []entity.PromoRule{
				{
					ID:        "01GCKY3SEJWJRN4YM5ANS599QP",
					Type:      entity.ITEM_BOUGHT_BY_X,
					ProductID: &googleHomeID,
					XCount:    &value_3,
				},
			},
			PromoActions: []entity.PromoAction{
				{
					ID:                 "01GCKY3SEJWJRN4YM5ANS599QP",
					Type:               entity.ITEM_DISCOUNT_FOR_X,
					ProductID:          &googleHomeID,
					XCount:             &value_1,
					DiscountPercentage: &value_100,
				},
			},
		},
		{
			ID:   "01GCKS158QDT62WNGMNYHKS4D8",
			Name: "Alexa Speaker Promo",
			Code: "ALEXA-01",
			PromoRules: []entity.PromoRule{
				{
					ID:        "01GCKY44256QSMAJDS73V2YAV2",
					Type:      entity.ITEM_BOUGHT_BY_X,
					ProductID: &alexaSpeakerID,
					XCount:    &value_3,
				},
			},
			PromoActions: []entity.PromoAction{
				{
					ID:                 "01GCKY44256QSMAJDS73V2YAV2",
					Type:               entity.ITEM_DISCOUNT_FOR_X,
					ProductID:          &alexaSpeakerID,
					XCount:             &value_9999,
					DiscountPercentage: &value_10,
				},
			},
		},
	}
}
