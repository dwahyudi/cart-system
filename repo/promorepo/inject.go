package promorepo

import (
	"cart-system/infra"
	"sync"
)

var (
	promoRepoOnce sync.Once
	promoRepoInj  PromoRepo
)

func NewPromoRepo(infra infra.Infra) PromoRepo {
	promoRepoOnce.Do(func() {
		promoRepoInj = &samplePromoRepo{}
	})
	return promoRepoInj
}
