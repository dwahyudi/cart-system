package cartdomainrepo

import (
	"cart-system/entity"
	"context"
)

type (
	CartDomainRepo interface {
		AddToCart(context.Context, entity.AddToCartReq) (entity.Cart, error)
		RemoveFromCart(context.Context, entity.RemoveFromCartReq) (entity.Cart, error)
		GetCartByID(context.Context, string) (entity.Cart, error)

		ApplyPromo(ctx context.Context, cart_id string, code string) (entity.Cart, error)
		RemovePromo(ctx context.Context, cart_id string) (entity.Cart, error)
	}
)
