package cartdomainrepo

import (
	"cart-system/entity"
	"cart-system/service/calc"
	"context"
	"sync"

	"github.com/oklog/ulid/v2"
)

func (c *cartMemory) adjustAddCartLine(
	ctx context.Context, cart *entity.Cart, product entity.Product, req entity.AddToCartReq,
) error {
	var cartLine entity.CartLine
	var index int

	for i, memoryCartLine := range cart.CartLines {
		if memoryCartLine.ProductID == product.ID {
			cartLine = *memoryCartLine
			index = i
		}
	}

	if cartLine.ID == "" {
		newCartLine, err := c.addNewCartLine(ctx, product, req)
		if err != nil {
			return err
		}
		cart.CartLines = append(cart.CartLines, &newCartLine)
	} else {
		c.updateCartLine(ctx, *cart, cartLine, index, product, req.ByQuantity)
	}

	return nil
}

func (c *cartMemory) addNewCartLine(
	ctx context.Context, product entity.Product, req entity.AddToCartReq,
) (entity.CartLine, error) {
	subtotal := calc.RoundFloat(req.ByQuantity*product.Price, 2)
	return entity.CartLine{
		ID:              ulid.Make().String(),
		ProductID:       product.ID,
		Quantity:        req.ByQuantity,
		Subtotal:        subtotal,
		ProductSnapshot: product,
	}, nil
}

func (c *cartMemory) updateCart(ctx context.Context, req entity.AddToCartReq) (entity.Cart, error) {
	var cart entity.Cart

	cart, err := c.GetCartByID(ctx, *req.CartID)
	if err != nil {
		return cart, err
	}

	cart.Mutex.Lock()

	product, err := c.productRepo.GetProductByID(ctx, req.ProductID)
	if err != nil {
		return cart, err
	}

	err = c.adjustAddCartLine(ctx, &cart, product, req)
	if err != nil {
		return cart, err
	}

	if err = c.calculateSubTotal(ctx, &cart); err != nil {
		return cart, err
	}

	cart.Mutex.Unlock()

	return cart, nil
}

func (c *cartMemory) addNewCart(ctx context.Context, req entity.AddToCartReq) (entity.Cart, error) {
	product, err := c.productRepo.GetProductByID(ctx, req.ProductID)
	if err != nil {
		return entity.Cart{}, err
	}

	newCartLine := entity.CartLine{
		ID:              ulid.Make().String(),
		ProductID:       req.ProductID,
		Quantity:        req.ByQuantity,
		Subtotal:        product.Price * req.ByQuantity,
		ProductSnapshot: product,
	}

	newCart := entity.Cart{
		ID:        ulid.Make().String(),
		Subtotal:  product.Price * req.ByQuantity,
		CartLines: []*entity.CartLine{&newCartLine},
		Mutex:     &sync.Mutex{},
	}

	return newCart, nil
}
