package cartdomainrepo

import (
	"cart-system/entity"
	"cart-system/infra"
	"cart-system/repo/productrepo"
	"cart-system/repo/promorepo"
	"sync"
)

var (
	cartDomainRepoOnce sync.Once
	cartDomainRepoInj  CartDomainRepo
)

func NewCartDomainRepo(infra infra.Infra) CartDomainRepo {
	cartDomainRepoOnce.Do(func() {
		cartDomainRepoInj = &cartMemory{
			carts:       make(map[string]*entity.Cart),
			productRepo: productrepo.NewProductRepo(infra),
			promoRepo:   promorepo.NewPromoRepo(infra),
		}
	})

	return cartDomainRepoInj
}
