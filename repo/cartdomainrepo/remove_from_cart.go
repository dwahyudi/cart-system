package cartdomainrepo

import (
	"cart-system/entity"
	"context"
	"errors"
)

func (c *cartMemory) removeFreeItems(ctx context.Context, cart *entity.Cart) error {
	indexesToBeRemoved := make([]int, 0)

	for i, memoryCartLine := range cart.CartLines {
		if memoryCartLine.FreeItem {
			indexesToBeRemoved = append(indexesToBeRemoved, i)
		}
	}

	for _, index := range indexesToBeRemoved {
		newCartLines := append(cart.CartLines[:index], cart.CartLines[index+1:]...)
		c.carts[cart.ID].CartLines = newCartLines
	}

	return nil
}

func (c *cartMemory) adjustRemoveCartLine(
	ctx context.Context, cart entity.Cart, product entity.Product, req entity.RemoveFromCartReq,
) error {
	var cartLine entity.CartLine
	var index int

	for i, memoryCartLine := range cart.CartLines {
		if memoryCartLine.ProductID == product.ID {
			cartLine = *memoryCartLine
			index = i
		}
	}

	if req.ToQuantity > cartLine.Quantity {
		return errors.New("cannot remove to quantity larger than current size")
	} else if req.ToQuantity > 0 {
		diff := cartLine.Quantity - req.ToQuantity
		c.updateCartLine(ctx, cart, cartLine, index, product, -diff)
	} else if req.ToQuantity < 1 {
		newCartLines := append(cart.CartLines[:index], cart.CartLines[index+1:]...)
		c.carts[cart.ID].CartLines = newCartLines
	}

	return nil
}
