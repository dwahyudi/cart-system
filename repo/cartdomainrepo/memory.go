package cartdomainrepo

import (
	"cart-system/entity"
	"cart-system/repo/productrepo"
	"cart-system/repo/promorepo"
	"cart-system/service/calc"
	"context"
	"errors"
)

type (
	cartMemory struct {
		carts       map[string]*entity.Cart // in-memory carts
		productRepo productrepo.ProductRepo
		promoRepo   promorepo.PromoRepo
	}
)

func (c *cartMemory) AddToCart(ctx context.Context, req entity.AddToCartReq) (entity.Cart, error) {
	var err error
	var cart entity.Cart

	if req.CartID == nil {
		cart, err = c.addNewCart(ctx, req)
	} else {
		cart, err = c.updateCart(ctx, req)
	}
	if err != nil {
		return cart, err
	}

	c.carts[cart.ID] = &cart

	return cart, err
}

func (c *cartMemory) RemoveFromCart(ctx context.Context, req entity.RemoveFromCartReq) (entity.Cart, error) {
	var cart entity.Cart

	cart, err := c.GetCartByID(ctx, req.CartID)
	if err != nil {
		return cart, err
	}

	cart.Mutex.Lock()

	product, err := c.productRepo.GetProductByID(ctx, req.ProductID)
	if err != nil {
		return cart, err
	}

	err = c.adjustRemoveCartLine(ctx, cart, product, req)
	if err != nil {
		return cart, err
	}

	cart2 := c.carts[cart.ID]
	if err = c.calculateSubTotal(ctx, cart2); err != nil {
		return cart, err
	}

	cart.Mutex.Unlock()

	return *c.carts[cart.ID], nil
}

func (c *cartMemory) ApplyPromo(ctx context.Context, cart_id string, code string) (entity.Cart, error) {
	var cart entity.Cart

	if cart.PromoID != "" {
		return cart, errors.New("cart has promo applied")
	}

	cart, err := c.GetCartByID(ctx, cart_id)
	if err != nil {
		return cart, err
	}

	promo, err := c.promoRepo.GetPromoByCode(ctx, code)
	if err != nil {
		return cart, err
	}

	cart.Mutex.Lock()

	eligible, err := c.promoRepo.IsEligible(ctx, promo, cart)
	if err != nil {
		return cart, err
	}

	if eligible {
		c.promoRepo.PromoActivate(ctx, promo, &cart)
	}

	c.carts[cart.ID] = &cart

	cart.Mutex.Unlock()

	return cart, nil
}

func (c *cartMemory) RemovePromo(ctx context.Context, cart_id string) (entity.Cart, error) {
	var cart entity.Cart

	cart, err := c.GetCartByID(ctx, cart_id)
	if err != nil {
		return cart, err
	}

	cart.Mutex.Lock()

	err = c.resetDiscount(ctx, &cart)
	if err != nil {
		return cart, err
	}

	err = c.removeFreeItems(ctx, &cart)
	if err != nil {
		return cart, err
	}

	cart.Mutex.Unlock()

	return cart, nil
}

func (c *cartMemory) GetCartByID(ctx context.Context, cartID string) (entity.Cart, error) {
	cart, ok := c.carts[cartID]
	if !ok {
		return *cart, errors.New("cart not found")
	}

	c.calculateSubTotal(ctx, cart)

	return *cart, nil
}

func (c *cartMemory) calculateSubTotal(
	ctx context.Context, cart *entity.Cart,
) error {
	subtotal := 0.0

	for _, cartLine := range cart.CartLines {
		subtotal += cartLine.Subtotal
	}

	cart.Subtotal = calc.RoundFloat(subtotal, 2)

	return nil
}

func (c *cartMemory) resetDiscount(ctx context.Context, cart *entity.Cart) error {
	cart.Discounts = []entity.Discount{}
	cart.PromoID = ""

	return nil
}

func (c *cartMemory) updateCartLine(
	ctx context.Context, cart entity.Cart,
	cartLine entity.CartLine, index int,
	product entity.Product, diff float64,
) error {
	newQuantity := cartLine.Quantity + diff
	if newQuantity < 1 {
		newQuantity = 0
	}
	subtotal := calc.RoundFloat(newQuantity*product.Price, 2)
	cart.CartLines[index].Quantity = newQuantity
	cart.CartLines[index].Subtotal = subtotal
	cart.CartLines[index].ProductSnapshot = product

	return nil
}
