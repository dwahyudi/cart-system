package productrepo

import (
	"cart-system/entity"
	"context"
)

type (
	ProductRepo interface {
		GetProducts(ctx context.Context, cursor *string) ([]entity.Product, error)
		GetProductByID(ctx context.Context, ID string) (entity.Product, error)
	}
)
