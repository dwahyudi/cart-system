package productrepo

import (
	"cart-system/entity"
	"context"
)

type (
	sampleProductRepo struct {
	}
)

func (s *sampleProductRepo) GetProducts(
	ctx context.Context, cursor *string,
) ([]entity.Product, error) {
	return s.sampleProducts(ctx), nil
}

func (s *sampleProductRepo) GetProductByID(
	ctx context.Context, ID string,
) (entity.Product, error) {
	var product entity.Product

	for _, sampleProduct := range s.sampleProducts(ctx) {
		if sampleProduct.ID == ID {
			product = sampleProduct
		}
	}

	return product, nil
}

func (s *sampleProductRepo) sampleProducts(ctx context.Context) []entity.Product {
	return []entity.Product{
		{
			ID:          "01GCKY3SEJWJRN4YM5ANS599QP",
			Sku:         "120P90",
			Name:        "Google Home",
			Price:       49.99,
			StockInHand: 10,
		},
		{
			ID:          "01GCKY3CQC6J71F3WF99YS52EP",
			Sku:         "43N23P",
			Name:        "Macbook Pro",
			Price:       5399.99,
			StockInHand: 5,
		},
		{
			ID:          "01GCKY44256QSMAJDS73V2YAV2",
			Sku:         "A304SD",
			Name:        "Alexa Speaker",
			Price:       109.5,
			StockInHand: 10,
		},
		{
			ID:          "01GCKY4FPQX4HGM9P5A3MZZE97",
			Sku:         "234234",
			Name:        "Raspberry Pi B",
			Price:       30,
			StockInHand: 2,
		},
	}
}
