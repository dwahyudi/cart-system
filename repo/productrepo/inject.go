package productrepo

import (
	"cart-system/infra"
	"sync"
)

var (
	productRepoOnce sync.Once
	productRepoInj  ProductRepo
)

func NewProductRepo(infra infra.Infra) ProductRepo {
	productRepoOnce.Do(func() {
		productRepoInj = &sampleProductRepo{}
		// productRepoInj = &postgreProductRepo{}
	})

	return productRepoInj
}
