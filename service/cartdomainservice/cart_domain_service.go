package cartdomainservice

import (
	"cart-system/entity"
	"cart-system/repo/cartdomainrepo"
	"context"
)

type (
	CartDomainService interface {
		AddToCart(context.Context, entity.AddToCartReq) (entity.Cart, error)
		RemoveFromCart(context.Context, entity.RemoveFromCartReq) (entity.Cart, error)
		GetCartByID(context.Context, string) (entity.Cart, error)
		ApplyPromo(ctx context.Context, cartID string, code string) (entity.Cart, error)
		RemovePromo(ctx context.Context, cartID string) (entity.Cart, error)
	}

	cartDomainService struct {
		cartDomainRepo cartdomainrepo.CartDomainRepo
	}
)

func (c *cartDomainService) AddToCart(ctx context.Context, req entity.AddToCartReq) (entity.Cart, error) {
	cart, err := c.cartDomainRepo.AddToCart(ctx, req)
	if err != nil {
		return entity.Cart{}, err
	}

	return cart, nil
}

func (c *cartDomainService) RemoveFromCart(ctx context.Context, req entity.RemoveFromCartReq) (entity.Cart, error) {
	cart, err := c.cartDomainRepo.RemoveFromCart(ctx, req)
	if err != nil {
		return entity.Cart{}, err
	}

	return cart, nil
}

func (c *cartDomainService) GetCartByID(ctx context.Context, cartID string) (entity.Cart, error) {
	cart, err := c.cartDomainRepo.GetCartByID(ctx, cartID)
	if err != nil {
		return entity.Cart{}, err
	}

	return cart, nil
}

func (c *cartDomainService) ApplyPromo(ctx context.Context, cartID string, code string) (entity.Cart, error) {
	cart, err := c.cartDomainRepo.ApplyPromo(ctx, cartID, code)
	if err != nil {
		return entity.Cart{}, err
	}

	return cart, nil
}

func (c *cartDomainService) RemovePromo(ctx context.Context, cartID string) (entity.Cart, error) {
	cart, err := c.cartDomainRepo.RemovePromo(ctx, cartID)
	if err != nil {
		return entity.Cart{}, err
	}

	return cart, nil
}
