package cartdomainservice

import (
	"cart-system/infra"
	"cart-system/repo/cartdomainrepo"
	"sync"
)

var (
	cartDomainServiceOnce sync.Once
	cartDomainServiceInj  CartDomainService
)

func NewCartDomainService(infra infra.Infra) CartDomainService {
	cartDomainServiceOnce.Do(func() {
		cartDomainServiceInj = &cartDomainService{
			cartDomainRepo: cartdomainrepo.NewCartDomainRepo(infra),
		}
	})
	return cartDomainServiceInj
}
