package productservice

import (
	"cart-system/infra"
	"cart-system/repo/productrepo"
	"sync"
)

var (
	productServiceOnce sync.Once
	productServiceInj  ProductService
)

func NewProductService(infra infra.Infra) ProductService {
	productServiceOnce.Do(func() {
		productServiceInj = &productService{
			productRepo: productrepo.NewProductRepo(infra),
		}
	})
	return productServiceInj
}
