package productservice

import (
	"cart-system/entity"
	"cart-system/repo/productrepo"
	"context"
)

type (
	ProductService interface {
		GetProducts(ctx context.Context, cursor *string) ([]entity.Product, error)
	}

	productService struct {
		productRepo productrepo.ProductRepo
	}
)

func (p *productService) GetProducts(
	ctx context.Context, cursor *string,
) ([]entity.Product, error) {
	products, err := p.productRepo.GetProducts(ctx, cursor)
	if err != nil {
		return nil, err
	}

	return products, nil
}
