package e2e_test

import (
	"cart-system/entity"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
)

func TestNewCartFromEmpty(t *testing.T) {
	cl := resty.New()

	requestBody := `{"query":"mutation AddToCart {\n  addToCart(input: {product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", by_quantity: 2}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var cart entity.CartAddToCartResponse
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 10799.98, cart.Data.AddToCart.Subtotal)
	assert.Equal(t, 2.0, cart.Data.AddToCart.CartLines[0].Quantity)
	assert.Equal(t, 10799.98, cart.Data.AddToCart.CartLines[0].Subtotal)

	cartId := cart.Data.AddToCart.ID

	requestBodyTmp := `{"query":"query GetCartByID {\n  getCartByID(cart_id: \"%s\") {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      quantity\n      subtotal\n    }\n  }\n}\n","operationName":"GetCartByID"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var cart2 entity.GetCartByIDResponse
	err = json.Unmarshal(resp.Body(), &cart2)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 10799.98, cart2.Data.GetCartByID.Subtotal)
	assert.Equal(t, 2.0, cart2.Data.GetCartByID.CartLines[0].Quantity)
	assert.Equal(t, 10799.98, cart2.Data.GetCartByID.CartLines[0].Subtotal)
}

func TestAddDifferentItemsAndRemoveSomeItems(t *testing.T) {
	cl := resty.New()

	requestBody := `{"query":"mutation AddToCart {\n  addToCart(input: {product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", by_quantity: 2}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var cart entity.CartAddToCartResponse
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	// 5399.99 * 2
	assert.Equal(t, 10799.98, cart.Data.AddToCart.Subtotal)
	assert.Equal(t, 2.0, cart.Data.AddToCart.CartLines[0].Quantity)
	assert.Equal(t, 10799.98, cart.Data.AddToCart.CartLines[0].Subtotal)

	cartId := cart.Data.AddToCart.ID

	requestBodyTmp := `{"query":"mutation AddToCart {\n  addToCart(input: {cart_id: \"%s\", product_id:\"01GCKY44256QSMAJDS73V2YAV2\", by_quantity: 3}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 11128.48, cart.Data.AddToCart.Subtotal)
	assert.Equal(t, 2.0, cart.Data.AddToCart.CartLines[0].Quantity)
	assert.Equal(t, 10799.98, cart.Data.AddToCart.CartLines[0].Subtotal)
	assert.Equal(t, 3.0, cart.Data.AddToCart.CartLines[1].Quantity)
	assert.Equal(t, 328.5, cart.Data.AddToCart.CartLines[1].Subtotal)

	requestBodyTmp = `{"query":"mutation AddToCart {\n  addToCart(input: {cart_id: \"%s\", product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", by_quantity: 4}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 32728.44, cart.Data.AddToCart.Subtotal)
	assert.Equal(t, 6.0, cart.Data.AddToCart.CartLines[0].Quantity)
	assert.Equal(t, 32399.94, cart.Data.AddToCart.CartLines[0].Subtotal)
	assert.Equal(t, 3.0, cart.Data.AddToCart.CartLines[1].Quantity)
	assert.Equal(t, 328.5, cart.Data.AddToCart.CartLines[1].Subtotal)

	requestBodyTmp = `{"query":"mutation RemoveFromCart {\n  removeFromCart(input: {cart_id: \"%s\", product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", to_quantity: 1}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"RemoveFromCart"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var cart2 entity.CartRemoveFromCartResponse
	err = json.Unmarshal(resp.Body(), &cart2)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 5728.49, cart2.Data.RemoveFromCart.Subtotal)
	assert.Equal(t, 1.0, cart2.Data.RemoveFromCart.CartLines[0].Quantity)
	assert.Equal(t, 5399.99, cart2.Data.RemoveFromCart.CartLines[0].Subtotal)
	assert.Equal(t, 3.0, cart2.Data.RemoveFromCart.CartLines[1].Quantity)
	assert.Equal(t, 328.5, cart2.Data.RemoveFromCart.CartLines[1].Subtotal)

	requestBodyTmp = `{"query":"mutation RemoveFromCart {\n  removeFromCart(input: {cart_id: \"%s\", product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", to_quantity: 0}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"RemoveFromCart"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	err = json.Unmarshal(resp.Body(), &cart2)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 328.5, cart2.Data.RemoveFromCart.Subtotal)
	assert.Equal(t, 3.0, cart2.Data.RemoveFromCart.CartLines[0].Quantity)
	assert.Equal(t, 328.5, cart2.Data.RemoveFromCart.CartLines[0].Subtotal)
	assert.Equal(t, 1, len(cart2.Data.RemoveFromCart.CartLines))

	requestBodyTmp = `{"query":"mutation RemoveFromCart {\n  removeFromCart(input: {cart_id: \"%s\", product_id:\"01GCKY44256QSMAJDS73V2YAV2\", to_quantity: 0}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"RemoveFromCart"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	err = json.Unmarshal(resp.Body(), &cart2)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 0.0, cart2.Data.RemoveFromCart.Subtotal)
	assert.Equal(t, 0, len(cart2.Data.RemoveFromCart.CartLines))

	requestBodyTmp = `{"query":"mutation AddToCart {\n  addToCart(input: {cart_id: \"%s\", product_id:\"01GCKY44256QSMAJDS73V2YAV2\", by_quantity: 7}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 766.5, cart.Data.AddToCart.Subtotal)
	assert.Equal(t, 7.0, cart.Data.AddToCart.CartLines[0].Quantity)
	assert.Equal(t, 1, len(cart.Data.AddToCart.CartLines))
}

func TestAddMultipleSameItems(t *testing.T) {
	cl := resty.New()

	requestBody := `{"query":"mutation AddToCart {\n  addToCart(input: {product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", by_quantity: 2}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var cart entity.CartAddToCartResponse
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	// 5399.99 * 2
	assert.Equal(t, 10799.98, cart.Data.AddToCart.Subtotal)
	assert.Equal(t, 2.0, cart.Data.AddToCart.CartLines[0].Quantity)
	assert.Equal(t, 10799.98, cart.Data.AddToCart.CartLines[0].Subtotal)

	cartId := cart.Data.AddToCart.ID

	requestBodyTmp := `{"query":"mutation AddToCart {\n  addToCart(input: {cart_id: \"%s\", product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", by_quantity: 3}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	// 5399.99 * 5
	assert.Equal(t, 26999.95, cart.Data.AddToCart.Subtotal)
	assert.Equal(t, 5.0, cart.Data.AddToCart.CartLines[0].Quantity)
	assert.Equal(t, 26999.95, cart.Data.AddToCart.CartLines[0].Subtotal)

	requestBodyTmp = `{"query":"mutation AddToCart {\n  addToCart(input: {cart_id: \"%s\", product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", by_quantity: 1}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	// 5399.99 * 6
	assert.Equal(t, 32399.94, cart.Data.AddToCart.Subtotal)
	assert.Equal(t, 6.0, cart.Data.AddToCart.CartLines[0].Quantity)
	assert.Equal(t, 32399.94, cart.Data.AddToCart.CartLines[0].Subtotal)
}
