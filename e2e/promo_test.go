package e2e_test

import (
	"cart-system/entity"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
)

func TestFreeItem(t *testing.T) {
	cl := resty.New()

	requestBody := `{"query":"mutation AddToCart {\n  addToCart(input: {product_id:\"01GCKY3CQC6J71F3WF99YS52EP\", by_quantity: 2}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var cart entity.CartAddToCartResponse
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	cartId := cart.Data.AddToCart.ID

	requestBodyTmp := `{"query":"mutation ApplyPromo {\n  applyPromo(cart_id:\"%s\", code: \"MACBOOK-01\") {\n    subtotal\n    cart_lines {\n      product_id\n      quantity\n      subtotal\n    }\n    discounts {\n      id\n      description\n      amount\n    }\n  }\n}","operationName":"ApplyPromo"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var applyDiscountResp entity.ApplyPromoResponse
	err = json.Unmarshal(resp.Body(), &applyDiscountResp)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, 10799.98, applyDiscountResp.Data.ApplyPromo.Subtotal)
	// Free item
	assert.Equal(t, 2, applyDiscountResp.Data.ApplyPromo.CartLines[1].Quantity)
	assert.Equal(t, 0.0, applyDiscountResp.Data.ApplyPromo.CartLines[1].Subtotal)
}

func TestItemDiscount(t *testing.T) {
	cl := resty.New()

	requestBody := `{"query":"mutation AddToCart {\n  addToCart(input: {product_id:\"01GCKY3SEJWJRN4YM5ANS599QP\", by_quantity: 3}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var cart entity.CartAddToCartResponse
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	cartId := cart.Data.AddToCart.ID

	requestBodyTmp := `{"query":"mutation ApplyPromo {\n  applyPromo(cart_id:\"%s\", code: \"GOOGLEHOME-01\") {\n    subtotal\n    cart_lines {\n      product_id\n      quantity\n      subtotal\n    }\n    discounts {\n      id\n      description\n      amount\n    }\n  }\n}","operationName":"ApplyPromo"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var applyDiscountResp entity.ApplyPromoResponse
	err = json.Unmarshal(resp.Body(), &applyDiscountResp)
	if err != nil {
		log.Fatalln(err)
	}

	// TODO: create total field in cart
	// 149.97 - 49.99 = 99.98
	assert.Equal(t, 149.97, applyDiscountResp.Data.ApplyPromo.Subtotal)
	assert.Equal(t, 49.99, applyDiscountResp.Data.ApplyPromo.Discounts[0].Amount)
}

func TestItemDiscountMultipleCount(t *testing.T) {
	cl := resty.New()

	requestBody := `{"query":"mutation AddToCart {\n  addToCart(input: {product_id:\"01GCKY44256QSMAJDS73V2YAV2\", by_quantity: 3}) {\n    id\n    subtotal\n    cart_lines {\n      product_id\n      subtotal\n      quantity\n      product_snapshot {\n        sku\n        name\n      }\n    }\n  }\n}","operationName":"AddToCart"}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var cart entity.CartAddToCartResponse
	err = json.Unmarshal(resp.Body(), &cart)
	if err != nil {
		log.Fatalln(err)
	}

	cartId := cart.Data.AddToCart.ID

	requestBodyTmp := `{"query":"mutation ApplyPromo {\n  applyPromo(cart_id:\"%s\", code: \"ALEXA-01\") {\n    subtotal\n    cart_lines {\n      product_id\n      quantity\n      subtotal\n    }\n    discounts {\n      id\n      description\n      amount\n    }\n  }\n}","operationName":"ApplyPromo"}`
	requestBody = fmt.Sprintf(requestBodyTmp, cartId)

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(BaseURL)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	var applyDiscountResp entity.ApplyPromoResponse
	err = json.Unmarshal(resp.Body(), &applyDiscountResp)
	if err != nil {
		log.Fatalln(err)
	}

	// TODO: create total field in cart
	// 328.5 - 32.85 = 295.65
	assert.Equal(t, 328.5, applyDiscountResp.Data.ApplyPromo.Subtotal)
	assert.Equal(t, 32.85, applyDiscountResp.Data.ApplyPromo.Discounts[0].Amount)
}
