package e2e_test

import (
	"cart-system/graph"
	"cart-system/graph/generated"
	"cart-system/infra"
	"cart-system/service/cartdomainservice"
	"cart-system/service/productservice"
	"log"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
)

const (
	BaseURL     = "http://localhost:7222/query"
	defaultPort = "7222"
)

// TODO: make product samples part of E2E fixtures built on mocked infra.
// TODO: refactor the server.go to decouple the graphql launcher.
func TestMain(m *testing.M) {
	log.Print("Initiating End to End tests...")

	log.Print("Setting up End to End tests...")
	prepared := make(chan (bool))
	go func() {
		infra := infra.NewInfra()
		resolver := &graph.Resolver{
			ProductService: productservice.NewProductService(infra),
			CartService:    cartdomainservice.NewCartDomainService(infra),
		}

		srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: resolver}))

		http.Handle("/", playground.Handler("GraphQL playground", "/query"))
		http.Handle("/query", srv)

		prepared <- true

		log.Printf("connect to http://localhost:%s/ for GraphQL playground", defaultPort)
		log.Fatal(http.ListenAndServe(":"+defaultPort, nil))
	}()

	<-prepared
	log.Print("Setting up Complete...")

	time.Sleep(2 * time.Duration(time.Second))

	log.Print("Running tests...")
	code := m.Run()

	os.Exit(code)
}
