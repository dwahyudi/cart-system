.PHONY: e2e api build

e2e:
	go test ./e2e -v -coverpkg=./... -shuffle=on -cover -count=1
api:
	go run server.go
build:
	go build