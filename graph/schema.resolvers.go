package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"cart-system/entity"
	"cart-system/graph/generated"
	"cart-system/graph/model"
	"cart-system/graph/transformer"
	"context"
)

// AddToCart is the resolver for the addToCart field.
func (r *mutationResolver) AddToCart(ctx context.Context, input model.NewItem) (*model.Cart, error) {
	cart, err := r.CartService.AddToCart(
		ctx, entity.AddToCartReq{CartID: input.CartID, ProductID: input.ProductID, ByQuantity: input.ByQuantity},
	)
	if err != nil {
		return nil, err
	}

	response := transformer.CartSvcToGraph(cart)

	return &response, nil
}

// RemoveFromCart is the resolver for the removeFromCart field.
func (r *mutationResolver) RemoveFromCart(ctx context.Context, input model.RemoveItem) (*model.Cart, error) {
	cart, err := r.CartService.RemoveFromCart(
		ctx, entity.RemoveFromCartReq{CartID: input.CartID, ProductID: input.ProductID, ToQuantity: input.ToQuantity},
	)

	if err != nil {
		return nil, err
	}

	response := transformer.CartSvcToGraph(cart)

	return &response, nil
}

// ApplyPromo is the resolver for the applyPromo field.
func (r *mutationResolver) ApplyPromo(ctx context.Context, cartID string, code string) (*model.Cart, error) {
	cart, err := r.CartService.ApplyPromo(ctx, cartID, code)
	if err != nil {
		return nil, err
	}

	response := transformer.CartSvcToGraph(cart)

	return &response, nil
}

// RemovePromo is the resolver for the removePromo field.
func (r *mutationResolver) RemovePromo(ctx context.Context, cartID string) (*model.Cart, error) {
	cart, err := r.CartService.RemovePromo(ctx, cartID)
	if err != nil {
		return nil, err
	}

	response := transformer.CartSvcToGraph(cart)

	return &response, nil
}

// GetProducts is the resolver for the getProducts field.
func (r *queryResolver) GetProducts(ctx context.Context, cursor *string) ([]*model.Product, error) {
	products, err := r.ProductService.GetProducts(ctx, cursor)
	if err != nil {
		return nil, err
	}

	response := transformer.ProductsSvcToGraph(products)
	return response, nil
}

// GetCartByID is the resolver for the getCartByID field.
func (r *queryResolver) GetCartByID(ctx context.Context, cartID string) (*model.Cart, error) {
	cart, err := r.CartService.GetCartByID(ctx, cartID)
	if err != nil {
		return nil, err
	}

	response := transformer.CartSvcToGraph(cart)

	return &response, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
