package graph

import (
	"cart-system/service/cartdomainservice"
	"cart-system/service/productservice"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	ProductService productservice.ProductService
	CartService    cartdomainservice.CartDomainService
}
