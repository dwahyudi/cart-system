package transformer

import (
	"cart-system/entity"
	"cart-system/graph/model"
)

func ProductsSvcToGraph(products []entity.Product) []*model.Product {
	response := make([]*model.Product, 0)
	for _, product := range products {
		response = append(response, &model.Product{
			ID:          product.ID,
			Sku:         product.Sku,
			Name:        product.Name,
			Price:       product.Price,
			StockInHand: product.StockInHand,
		})
	}
	return response
}

func CartSvcToGraph(cart entity.Cart) model.Cart {
	response := model.Cart{ID: cart.ID, Subtotal: cart.Subtotal}

	cartLinesResponse := make([]*model.CartLine, 0)
	for _, cartLine := range cart.CartLines {
		productSnapshot := cartLine.ProductSnapshot
		cartLinesResponse = append(cartLinesResponse, &model.CartLine{
			ID:        cartLine.ID,
			ProductID: cartLine.ProductID,
			Quantity:  cartLine.Quantity,
			Subtotal:  cartLine.Subtotal,
			ProductSnapshot: &model.Product{
				ID:    productSnapshot.ID,
				Sku:   productSnapshot.Sku,
				Name:  productSnapshot.Name,
				Price: productSnapshot.Price,
			},
		})
	}

	response.CartLines = cartLinesResponse

	discountsResponse := make([]*model.Discount, 0)
	for _, discount := range cart.Discounts {
		discountsResponse = append(discountsResponse, &model.Discount{
			ID:          discount.ID,
			Description: discount.Description,
			Amount:      discount.Amount,
		})
	}

	response.Discounts = discountsResponse

	return response
}
