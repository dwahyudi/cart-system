package entity

type CartAddToCartResponse struct {
	Data AddToCartData `json:"data"`
}

type AddToCartData struct {
	AddToCart AddToCart `json:"addToCart"`
}

type AddToCart struct {
	ID        string     `json:"id"`
	Subtotal  float64    `json:"subtotal"`
	CartLines []CartLine `json:"cart_lines"`
}

type RemoveFromCart struct {
	ID        string     `json:"id"`
	Subtotal  float64    `json:"subtotal"`
	CartLines []CartLine `json:"cart_lines"`
}

type CartRemoveFromCartResponse struct {
	Data RemoveFromCartData `json:"data"`
}

type RemoveFromCartData struct {
	RemoveFromCart RemoveFromCart `json:"removeFromCart"`
}

type GetCartByIDResponse struct {
	Data struct {
		GetCartByID struct {
			ID        string  `json:"id"`
			Subtotal  float64 `json:"subtotal"`
			CartLines []struct {
				ProductID string  `json:"product_id"`
				Quantity  float64 `json:"quantity"`
				Subtotal  float64 `json:"subtotal"`
			} `json:"cart_lines"`
		} `json:"getCartByID"`
	} `json:"data"`
}

type ApplyPromoResponse struct {
	Data struct {
		ApplyPromo struct {
			Subtotal  float64 `json:"subtotal"`
			CartLines []struct {
				ProductID string  `json:"product_id"`
				Quantity  int     `json:"quantity"`
				Subtotal  float64 `json:"subtotal"`
			} `json:"cart_lines"`
			Discounts []struct {
				ID          string  `json:"id"`
				Description string  `json:"description"`
				Amount      float64 `json:"amount"`
			} `json:"discounts"`
		} `json:"applyPromo"`
	} `json:"data"`
}
