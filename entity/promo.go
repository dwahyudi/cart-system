package entity

type PromoRuleType string
type PromoActionType string

const (
	ITEM_BOUGHT_BY_X PromoRuleType = "ITEM_BOUGHT_BY_X"
)

const (
	FREE_ITEM           PromoActionType = "FREE_ITEM"
	ITEM_DISCOUNT_FOR_X PromoActionType = "ITEM_DISCOUNT_FOR_X"
)

type (
	Promo struct {
		ID                  string
		Name                string
		Code                string
		PromoRules          []PromoRule
		PromoActions        []PromoAction
		MustSatisfyAllRules bool
	}

	PromoRule struct {
		ID        string
		Type      PromoRuleType
		ProductID *string
		XCount    *float64
	}

	PromoAction struct {
		ID                 string
		Type               PromoActionType
		ProductID          *string
		ProductRefID       *string
		XCount             *float64
		DiscountPercentage *float64
	}
)
