package entity

import "sync"

type (
	Cart struct {
		ID        string
		Subtotal  float64
		PromoID   string
		CartLines []*CartLine
		Discounts []Discount

		Mutex *sync.Mutex
	}

	CartLine struct {
		ID              string  `json:"id"`
		ProductID       string  `json:"product_id"`
		Quantity        float64 `json:"quantity"`
		Subtotal        float64 `json:"subtotal"`
		ProductSnapshot Product `json:"product_snapshot"`
		FreeItem        bool
	}

	Discount struct {
		ID          string
		Description string
		Amount      float64
	}

	AddToCartReq struct {
		CartID     *string
		ProductID  string
		ByQuantity float64
	}

	RemoveFromCartReq struct {
		CartID     string
		ProductID  string
		ToQuantity float64
	}
)
