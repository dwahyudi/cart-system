package entity

type (
	Product struct {
		ID          string
		Sku         string
		Name        string
		Price       float64
		StockInHand float64
	}
)
