package infra

import "sync"

var (
	infraOnce sync.Once
	infraInj  Infra
)

func NewInfra() Infra {
	infraOnce.Do(func() {
		infraInj = &infra{}
	})
	return infraInj
}
